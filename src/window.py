# window.py
#
# Copyright 2019 Felix Quill
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, GObject  # Libs for the GUI
import logging                          # Logging system
import sys                              # System Libs
from sympy.polys import Poly            # Library for Polinomial Operations
from sympy.abc import alpha, x
from sympy import GF, Matrix
from sympy.polys.galoistools import gf_irreducible
from sympy.polys import polytools
from sympy.solvers import solve
from sympy.core.add import Add
from src.mathutils import *
import numpy as np
import threading                        # some stuff in here takes for ever to compute - we don't want to freeze the app for this time


# init logging to get proper debug output
root = logging.getLogger()
root.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(name)s-%(levelname)s: %(asctime)s: %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)

#the progressbar must be everywhere accessible
progress_bar = None #It's unitialized because we will do this later …

#the encrypted entry must be everywhere accessible
entry_enc = None

public_key_filename = None
private_key_filename = None

# Create Application Window
@Gtk.Template(resource_path='/org/archyt/mceliece/window.ui')
class McelieceWindow(Gtk.ApplicationWindow):
    logger = logging.getLogger("APP")
    mceliece = None
    __gtype_name__ = 'McelieceWindow'
    btn_start = Gtk.Template.Child()
    btn_enc = Gtk.Template.Child()
    progress = Gtk.Template.Child()
    entry_n = Gtk.Template.Child()
    entry_k = Gtk.Template.Child()
    entry_t = Gtk.Template.Child()
    entry_pub_keyf = Gtk.Template.Child()
    entry_priv_keyf = Gtk.Template.Child()
    entry_to_enc = Gtk.Template.Child()
    entry_encrypted = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.btn_start.connect('button_press_event', self.on_btn_start)
        self.btn_enc.connect('button_press_event', self.on_btn_enc)
        global progress_bar
        progress_bar = self.progress
        global entry_enc
        entry_enc = self.entry_encrypted

    def on_btn_start(self, widget, event):
        self.logger.info("Hello World")
        n = int(self.entry_n.get_text())
        k = int(self.entry_k.get_text())
        t = int(self.entry_t.get_text())
        global private_key_filename
        global public_key_filename
        private_key_filename = self.entry_priv_keyf.get_text()
        public_key_filename = self.entry_pub_keyf.get_text()
        self.mceliece = McEliece(n, k, t)
        self.mceliece.generateKeys()

    def on_btn_enc(self, widget, event):
        self.logger.info("Starting Encryption …")
        global public_key_filename
        public_key_filename = self.entry_pub_keyf.get_text()
        public_key_file = public_key_filename
        self.logger.debug(f"public_key_file({public_key_file})")
        pub_key = np.load(public_key_file + ".npz")
        self.mceliece = McEliece(int(pub_key['m']), int(pub_key['n']), int(pub_key['t']))
        to_enc_txt = self.entry_to_enc.get_text()
        to_enc_bytes = np.array(to_enc_txt).tobytes()
        to_enc_txt = np.unpackbits(np.frombuffer(to_enc_bytes, dtype=np.uint8))
        self.logger.debug(f"to_enc_txt(\n{to_enc_txt})")
        to_enc_txt = np.trim_zeros(to_enc_txt)
        self.logger.info("POLYNOMIAL DEGREE: {}".format(max(0, len(to_enc_txt) - 1)))
        self.mceliece.encrypt(pub_key['Gp'], to_enc_txt)

#GObject.threads_init()
class McEliece(object):
    logger = logging.getLogger("McEliece")
    thread = None

    def __init__(self, m, n, t):
        self.m = m
        self.n = n
        self.t = t
        pass

    def generateKeys(self):
        cipher = Cipher(self.m, self.n, self.t)
        self.thread = threading.Thread(target=cipher.generate_random_keys)
        self.thread.start()

    def encrypt(self, Gp, to_enc_txt):
        cipher = Cipher(self.m, self.n, self.t)
        self.thread = threading.Thread(target=cipher.encrypt(Gp, to_enc_txt))
        self.thread.start()


class Cipher(object):
    logger = logging.getLogger("Chipher")
    def __init__(self, n, k, t):
        self.n = int(n)
        self.k = int(k)
        self.t = int(t)
        self.q = 2 #base powered by m
        self.logger.info(f"Chipher(n={self.n}, k={self.k}, t={self.t}, q={self.q}, q^t={self.q ** self.t})")
        self.G = None
        self.H = None
        #self.k = None
        self.P = None
        self.P_inv = None
        self.S = None
        self.S_inv = None
        self.Gp = None
        self.g_poly = None
        self.irr_poly = None

        progress_bar.pulse()
        pass

    def encrypt(self, Gp, to_enc_txt):
        self.Gp = Gp
        self.logger.debug(f"Gp(\n{self.Gp})")
        if self.Gp.shape[0] < len(to_enc_txt):
            raise Exception(f"Input is too large for current N. Should be {self.Gp.shape[0]}")
        #output = mceliece.encrypt(input_arr).to_numpy()
        self.logger.debug(f"msg: {to_enc_txt}")
        Cp = GF2Matrix.from_list(to_enc_txt)# * GF2Matrix.from_list(self.Gp)
        self.logger.debug(f"C': {to_enc_txt}")
        bits_to_flip = np.random.choice(len(Cp), size=self.t, replace=False)
        self.logger.debug(f"bits_to_flip: {bits_to_flip}")
        for b in bits_to_flip:
            Cp[b] = Cp[b].flip()
        self.logger.debug(f"C': {Cp}")
        output = Cp.to_numpy()
        output =np.array(output).flatten()
        output = np.packbits(np.array(output).astype(np.int)).tobytes()
        #output = output.decode("utf-8")
        #sys.stdout.buffer.write(output)
        output = output.decode('raw_unicode_escape') #I don't know if this is allways correct, maybe I should output the raw byte data
        self.logger.info(output)

        global entry_enc
        encrypted_entry = entry_enc
        encrypted_entry.set_text(output)


    def generate_random_keys(self):
        self.logger.debug(f"k({self.k})")
        generator = GoppaCodeGenerator(self.n, self.k, self.t)
        g, L = generator.gen()

        #self.G, self.H, self.g_poly, self.irr_poly = generator.gen()

        #progress_bar.pulse()

        #self.logger.info(f"G{self.G}")
        #self.logger.info(f"H\n{self.H}")

        #self.g_poly = np.array([(Poly(e, alpha) % self.irr_poly).trunc(2).all_coeffs()[::-1] for e in
        #                        self.g_poly.all_coeffs()[::-1]])
        #self.irr_poly = np.array(self.irr_poly.all_coeffs()[::-1])
        #self.logger.info(f"g_poly{self.g_poly}")
        #self.logger.info(f"irr_poly{self.irr_poly}")

        #progress_bar.pulse()

        #self.k = self.G.arr.shape[0]
        #self.logger.info(f"k({self.k})")

        #self.P = GF2Matrix.from_list(random_perm_matrix(self.n))
        #self.P_inv = self.P.inv()

        #self.logger.info(f"P_inv(\n{self.P_inv})")

        # Use of random defined elements is dangerous and can couse errors.
        # So we will try it again if something fails.
        # Btw. I'm wondering why this never happens with P?
        #S_generation_succsess = False
        #S_generation_fails = 0
        #while not S_generation_succsess and not S_generation_fails == 100:
        #    try:
        #        self.S = GF2Matrix.from_list(random_inv_matrix(self.k))
        #        self.S_inv = self.S.inv()
        #        S_generation_succsess = True
        #    except AttributeError:
        #        self.logger.debug("Failed to generate S - AttributeError")
        #        S_generation_fails += 1
        #    except ZeroDivisionError:
        #        self.logger.debug("Failed to generate S_inv - ZeroDivisionError")
        #        S_generation_fails += 1
        #if S_generation_fails == 100:
        #    raise Exception("Failed to generate S and S_inv. Please retry.")
        #self.logger.info(f"S_inv(\n{self.S_inv})")

        #progress_bar.pulse()

        #self.Gp = self.S * self.G * self.P
        #self.logger.info(f"Gp(\n{self.Gp})")

        #progress_bar.set_text("Writing keys")
        #progress_bar.set_fraction(0)

        #global private_key_filename
        #global public_key_filename
        #priv_key_file = private_key_filename
        #pub_key_file = public_key_filename
        #np.savez_compressed(priv_key_file, m=self.m, n=self.n, t=self.t, S=self.S, S_inv=self.S_inv, G=self.G, H=self.H,
        #                P=self.P, P_inv=self.P_inv, g_poly=self.g_poly, irr_poly=self.irr_poly)
        #progress_bar.set_fraction(0.50)
        #self.logger.info(f"Saved private key to: {priv_key_file}")
        #np.savez_compressed(pub_key_file, m=self.m, n=self.n, t=self.t, Gp=self.Gp)
        #self.logger.info(f"Saved public key to: {pub_key_file}")
        #progress_bar.set_fraction(1)

class GoppaCodeGenerator(object):
    logger = logging.getLogger("GoppaCodeGenerator")

    def __init__(self, n, k, t):
        self.n = n
        self.k = k
        self.t = t
        self.q = 2
        self.logger.info(f"GoppaCodeGenerator(n={self.n}, k={self.k}, t={self.t}, q={self.q}, q^m={self.q ** 8})")
    def gen(self):
        #g = Poly(alpha ** self.m + alpha + 1, alpha).set_domain(GF(self.q))
        #self.logger.debug(f"g(x) -> {g}")

        #Binary Goppa Codes are defined by a Polynomial g(x) over a finite field GF(2^m) [without multible zeros] <- not sure how to implement this
        g = Poly(alpha ** self.t + alpha + 1, alpha, domain=GF(self.q ** self.t)) #m = t; Warum muss jeder was anderes schreiben???
        self.logger.debug(f"g(x) -> {g}")

        g_coeffs = g.all_coeffs() #In order to calculte the roots of g(x) we need it's coeffs
        self.logger.debug(f"g_coeffs({g_coeffs})")

        #Now calculate the roots of g(x)
        g_roots = polytools.nroots(g, n=30, maxsteps=50, cleanup=True)#np.roots(g_coeffs)
        self.logger.debug(f"g_roots({g_roots})")

        #L = []
        #for l in g_coeffs:
        #    print(l)
        #    y = g.eval(l)
        #    self.logger.debug(y)
        #    if y not in g_roots and y != 0:
        #        L.append(l)

        #According to Wikipedia, we need ALL elements of GF(2**t). We can archive this by computing the roots of x^(GF(…))-x
        element_poly = Poly(x**(self.q**self.t)-x, x, domain=GF(self.q ** self.t))
        element_roots = polytools.nroots(element_poly, n=30, maxsteps=50, cleanup=True)
        self.logger.debug(f"element_roots({element_roots})")

        L = np.zeros([1, self.n])#Binary Goppa Codes consist on the code g and a List L, gefüllt mit Elementen von GF(2**m), die keine Wurzeln von g sind

        for i in range(0, self.n - 1):
            print(f"i={i}")
            for j in range(0, self.n - 1):
                print(f"j={j}")
                print(L)
                if isinstance(element_roots[i], Add):
                    print("root is of type Add!")
                    element_roots[i] = complex(element_roots[i].as_real_imag()[0], element_roots[i].as_real_imag()[1]).real
                    print(f"root is now {element_roots[i]}")
                #TODO eval with int or float …
                self.logger.debug(f"root is of type({type(element_roots[i])})")
                self.logger.debug(f"element_roots_i({element_roots[i]})")
                self.logger.debug(f"element_roots_j({element_roots[j]})")
                self.logger.debug(f"g_eval({g.eval(element_roots[i])})")
                if element_roots[i] == element_roots[j] and g.eval(element_roots[i]) != 0:#if g_coeffs[i] == g_coeffs[j] and g.eval(g_coeffs[i]) != 0:
                    L[0][i]=1
                break
                #else:
                    #L[0][i]=0

        #Next step is the Generator matrix
        print(self.k)
        identity = np.empty([self.k, self.k], dtype=int) #for this we need the identity / unit matrix
        for i in range(0, self.k):
            for j in range(0, self.k):
                if i == j:
                    identity[i][j] = 1
                else:
                    identity[i][j] = 0
        self.logger.debug(f"identyty(\n{identity})")

        #Now we compute the Parity-Check-Matrix, composed of the Vandermode matrix and a diagonal matrix
        V = np.empty([self.t-1, self.n-1], dtype=int) #V with t-1 rows and n-1 columns
        for row in range(0, self.t-1):
            for column in range(0, self.n-1):
                V[row][column] = L[0][column] ** row

        self.logger.debug(f"Vandermode(\n{V})")

        D = np.zeros([self.n-1, self.n-1], dtype=int) #D with n-1 rows and columns
        for row in range(0, self.n-1):
            for column in range(0, self.n-1):
                if row == column:
                    D[row][column] = 1/(g.eval(L[0][row]))

        self.logger.debug(f"Diagonal(\n{D})")

        #Now build H by multiplying V with D
        H = np.dot(V, D)

        self.logger.debug(f"Parity-Check(\n{H})")

        D = Matrix(H).nullspace()

        self.logger.debug(f"Generator-Code(\n{D})")

        return g, L
        #quit()
        """
        Original
        """

        irr_poly = Poly(alpha ** self.m + alpha + 1, alpha).set_domain(GF(self.q)) # Initialize an irreducible polynomial

        progress_bar.pulse()

        self.logger.debug("Checking for irreducible_poly …")
        if is_irreducible_poly(irr_poly, self.q):
            self.logger.debug("Evaluating ring …")
            ring = power_dict(self.q ** self.m, irr_poly, self.q) # Reduce the Polynom to it's factors -> together in their "ring"
        else:
            ring = []

        while len(ring) < self.q ** self.m -1:
            irr_poly = irreducible_poly(self.m, self.q, alpha)
            self.logger.debug("Evaluating ring …")
            ring = power_dict(self.q ** self.m, irr_poly, self.q)
            self.logger.info(f"irr(q_size: {len(ring)}): {irr_poly}")

        self.logger.debug(f"ring({ring})")

        g_poly = Poly(1, x)

        roots_num = max(0, self.q ** self.m - self.n - self.t)

        #g_roots = np.random.choice(range(self.q ** self.m - 1), roots_num, replace=False)
        g_roots = set()
        g_non_roots = list(set(range(self.q ** self.m - 1)) - set(g_roots))

        self.logger.debug(f"g_roots({len(g_roots)})={g_roots}")
        self.logger.debug(f"g_non_roots({len(g_non_roots)})={g_non_roots}")

        #This is verry CPU intensive and a later candidate for multitasking - so far we gonna use lower m and n for generation instead of 10 for both …
        for i in g_roots:
            g_poly = (g_poly * Poly(x + alpha ** i, x)).trunc(self.q) # Trunc = reduce the polynomial modulo q
        self.logger.debug(f"g_poly({g_poly})")

        #Degree means the biggest power of the polynomial. For example, a simple linear function is a polynomial with the power of 2.
        if g_poly.degree() < self.t:
            small_irr = None
            for i in range(100):
                progress_bar.set_text(f"Computing small_irr: {i}/{100}")
                progress_bar.set_fraction(i * 0.01)

                small_irr = irreducible_poly_ext_candidate(self.t - g_poly.degree(), irr_poly, self.q, x, non_roots=g_non_roots)
                self.logger.debug(f"irr_part_of_g={small_irr}")
                if small_irr.eval(0).is_zero or small_irr.eval(1).is_zero:
                    self.logger.debug(f'roots in trivial case 0:{small_irr.eval(0)} 1:{small_irr.eval(1)}')
                    continue
                first_root = first_alpha_power_root(small_irr, irr_poly, self.q)
                if first_root > 0:
                    self.logger.debug(f"alpha^{first_root} is a root of g(x)={small_irr}")
                    continue
                break
            else:
                #If you trust me or not: this exception will rise if the loop came to an end without calling 'break'
                raise Exception("irr poly not found")
            g_poly = (g_poly * small_irr).trunc(self.q)

        g_poly = reduce_to_alpha_power(g_poly, irr_poly, ring, self.q)
        self.logger.info(f"g(x)={g_poly}")
        self.logger.debug("^--- g_poly, btw")
        coeffs = g_poly.all_coeffs()
        self.logger.debug(f"coefficients of g_poly({coeffs})")

        first_root = first_alpha_power_root(g_poly, irr_poly, self.q, elements_to_check=g_non_roots)
        if first_root > 0:
            raise Exception(f"alpha^{first_root} is a root of g(x)={g_poly}")

        C = Matrix(self.t, self.t, lambda i, j: coeffs[j - i] if 0 <= j - i < self.t else 0)
        self.logger.debug(f"C={C}")
        X = Matrix(self.t, self.n, lambda i, j: (alpha ** ((j * (self.t - i - 1)) % self.n)))
        self.logger.debug(f"X={X}")
        Y = Matrix(self.n, self.n, lambda i, j: get_alpha_power(g_poly.eval(alpha ** g_non_roots[i]), irr_poly, ring, self.q, neg=True) if i == j else 0)
        self.logger.debug(f"Y={Y}")

        H = C * X * Y
        H = Matrix(self.t, self.n, lambda i, j: get_alpha_power(H[i, j], irr_poly, ring, self.q))
        self.logger.debug(f"H={H}")

        #anyway, lets convert that into binary …
        H_bin = np.array(
            [np.column_stack([get_binary_from_alpha(e, irr_poly, self.q) for e in line]) for line in
             H.tolist()]).astype(GF2)
        H_bin = GF2Matrix.from_list(H_bin.reshape(-1, H.shape[1])) #This is not an override of H_bin!!!
        self.logger.info(f"H_bin=\n{H_bin}")

        H_nullspace, nullity = H_bin.nullspace()
        self.logger.debug(f"H_nullspace({nullity})=\n{H_nullspace})")

        G = GF2Matrix(H_nullspace.T()[:nullity])
        self.logger.info(f"G=\n{G}")
        self.logger.debug(f"G*H^T=\n{G * H_bin.T()}")

        return G, H_bin, g_poly, irr_poly

