from sympy.polys.galoistools import gf_irreducible_p, gf_irreducible
from sympy.polys import Poly
from sympy.abc import alpha, x
from sympy import ZZ, GF
import numpy as np
from flint import *

#for large powers
def bin_exp(x, k):
    result = 1
    for bit in bin(k):
        result = result ** 2
        if bit == "1":
            result *= x
    return result

def irreducible_poly(m, p, var):
    return Poly([int(c.numerator) for c in gf_irreducible(m, p, ZZ)], var)

def is_irreducible_poly(poly, p):
    return gf_irreducible_p([int(c) for c in poly.all_coeffs()], p, ZZ)

def irreducible_poly_ext_candidate(m, irr_poly, p, var, non_roots=None):
    elems = [0, 1, alpha]
    for e in non_roots:
        elems.append(alpha ** e)
    return Poly(np.concatenate([np.random.choice(elems[1:], size=1, replace=True),
                                np.random.choice(elems, size=m, replace=True)], axis=0), var)

def power_dict(n, irr, p):
    result = {(1,): 0}
    test_poly = Poly(1, alpha)
    for i in range(1, n - 1):
        test_poly = (Poly(Poly(alpha, alpha) * test_poly, alpha) % irr).set_domain(GF(p))
        if tuple(test_poly.all_coeffs()) in result:
            return result
        result[tuple(test_poly.all_coeffs())] = i
    return result

def first_alpha_power_root(poly, irr_poly, p, elements_to_check=None):
    poly = Poly([(Poly(coeff, alpha) % irr_poly).trunc(p).as_expr() for coeff in poly.all_coeffs()], x)
    test_poly = Poly(1, alpha)
    for i in range(1, p ** irr_poly.degree()):
        test_poly = (Poly(Poly(alpha, alpha) * test_poly, alpha) % irr_poly).set_domain(GF(p))
        if elements_to_check is not None and i not in elements_to_check:
            continue
        value = Poly((Poly(poly.eval(test_poly.as_expr()), alpha) % irr_poly), alpha).trunc(p)
        if value.is_zero:
            return i
    return -1

def get_alpha_power(poly, irr_poly, quotient, p, neg=False):
    poly = (Poly(poly, alpha) % irr_poly).trunc(p)
    if poly.is_zero:
        return 0
    power = quotient[tuple(poly.all_coeffs())]
    if neg:
        power = len(quotient) - power
    return alpha ** power

def reduce_to_alpha_power(poly, irr_poly, quotient, p):
    return Poly([get_alpha_power(coeff, irr_poly, quotient, p) for coeff in poly.all_coeffs()], x)

def get_binary_from_alpha(poly, irr_poly, p):
    poly = (Poly(poly, alpha) % irr_poly).trunc(p)
    result = np.full((irr_poly.degree(),), GF2(0))
    result[:len(poly.all_coeffs())] = [GF2(e) for e in poly.all_coeffs()[::-1]]
    return result

def random_perm_matrix(n):
    return np.array([[1 if i == x else 0 for i in range(n)] for x in np.random.permutation(n)])

def random_inv_matrix(n):
    for i in range(1, 1000):
        try:
            candidate = np.random.randint(2, size=(n, n))
            det = int(round(np.linalg.det(candidate)))
            #print("Generating random matrix (det={}). Try {}...".format(det, i))
            if det % 2 == 1:
                return candidate
        except ValueError:
            pass
    return None

class GF2():

    def __init__(self, n):
        self.n = int(n)

    def __add__(self, other):
        return GF2(self.n ^ other.n)

    def __sub__(self, other):
        return GF2(self.n ^ other.n)

    def __mul__(self, other):
        return GF2(self.n & other.n)

    def __truediv__(self, other):
        return self * other.inv()

    def __neg__(self):
        return self

    def __eq__(self, other):
        if isinstance(other, GF2):
            return self.n == other.n
        if self.n == int(other):
            return True
        return False

    def __abs__(self):
        return abs(self.n)

    def __str__(self):
        return str(self.n)

    def __repr__(self):
        return self.__str__()

    def __int__(self):
        return self.n

    def __divmod__(self, divisor):
        q, r = divmod(self.n, divisor.n)
        return (GF2(q), GF2(r))

    def flip(self):
        return GF2(1 if self.n == 0 else 0)

    def inv(self):
        x, y, d = ext_euclid(self.n, 2)
        return GF2(x)


class GF2Matrix:

    def __init__(self, arr):
        self.arr = np.asarray(arr, dtype=GF2)

    @staticmethod
    def from_list(list):
        if len(list.shape) == 1:
            return GF2Matrix(np.array([GF2(x) for x in list]))
        return GF2Matrix(np.array([[GF2(x) for x in line] for line in list]))

    @staticmethod
    def from_flint(list):
        return GF2Matrix(np.array([GF2(e) for e in list.entries()]).reshape(list.nrows(), list.ncols()))

    def to_numpy(self):
        return np.array([int(x) for x in self.arr.flat])

    def __add__(self, other):
        return GF2Matrix(self.arr + other.arr)

    def __sub__(self, other):
        return GF2Matrix(self.arr + other.arr)

    def __mul__(self, other):
        return GF2Matrix(np.dot(self.arr, other.arr))

    def __truediv__(self, other):
        return self * other.inv()

    def __neg__(self):
        return self

    def __eq__(self, other):
        return isinstance(other, GF2Matrix) and self.arr == other.arr

    def __abs__(self):
        return abs(self.arr)

    def __str__(self):
        return str(self.arr)

    def __repr__(self):
        return self.__str__()

    def __len__(self):
        return len(self.arr)

    def __getitem__(self, item):
        return self.arr[item]

    def __setitem__(self, key, value):
        self.arr[key] = value

    def T(self):
        return GF2Matrix(self.arr.T)

    def to_flint(self):
        return nmod_mat(self.arr.shape[0], self.arr.shape[1], [int(e) for e in self.arr.flatten()], 2)

    def nullspace(self):
        X, nullity = self.to_flint().nullspace()
        return GF2Matrix.from_flint(X), nullity

    def rref(self):
        X, rank = self.to_flint().rref()
        return GF2Matrix.from_flint(X), rank

    def inv(self):
        print(f"inv arr.shape[0]({self.arr.shape[0]})")
        print(f"inv arr.shape[1]({self.arr.shape[1]})")
        eye = nmod_mat(self.arr.shape[0], self.arr.shape[1], [int(e) for e in np.eye(self.arr.shape[0], self.arr.shape[1]).flatten()], 2)
        return GF2Matrix.from_flint(self.to_flint().solve(eye))

    def inv2(self):
        if int(round(np.linalg.det(self.arr.astype(int)))) % 2 != 1:
            raise Exception("Matrix not inversible.")
        return GF2Matrix(
            rref(
                GF2Matrix(
                    np.append(
                        self.arr,
                        GF2Matrix.from_list(np.eye(self.arr.shape[0]).astype(int)).arr, axis=1)
                )
            )[:, self.arr.shape[0]:])

