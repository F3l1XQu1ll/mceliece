import numpy as np
from sympy import GF, Matrix
from sympy.abc import x, a
from sympy.polys.polytools import *
from flint import *


def findDegree(e):
    if e:
        result = -1
        while e:
            e = e >> 1
            result = result + 1
        return result
    else:
        return 0


def findPoly(e):
    e_Degree = findDegree(e)
    result = ''
    if e == 0:
        return '0'

    for i in range(e_Degree, 0, -1):
        if (1 << i) & e:
            result = result + (' x**' + repr(i))
    if 1 & e:
        result = result + ' ' + repr(1)
    return result.strip().replace(' ', ' + ')


def split_poly(f):
    to_factor_poly = str(f)
    to_factor_poly = to_factor_poly.split(")*")
    for i in range(0, len(to_factor_poly)):
        item = to_factor_poly[i]
        if item.endswith(")"):
            continue
        else:
            to_factor_poly[i] += ")"
    return to_factor_poly


def get_alpha(f):
    alpha = None
    for factor in f:
        if Poly(factor).set_domain(GF(2)).degree() == 4:
            alpha = factor
            return alpha


def findAlphas(alpha):
    L = []
    # L.append("0")
    for i in range(2, 15):
        L.append(f"a**{i}")
    return L


def buildGoppaIrrPoly(alphas, degree):
    g = "a*x**0"
    for i in range(1, degree + 1):
        alpha = alphas[i]
        g += f"+{alpha}*x**{i}"
    return g


def convertGEToBin(ge, a_def):
    ge = str(ge)
    aa = ge.replace("Poly(", "")
    aa = aa.replace(", a, modulus=16)", "")
    aa = aa.split(" + ")
    # print(aa)

    for i in range(0, len(aa)):
        if aa[i].startswith("2"):
            aa[i] = aa[i].replace("2*", "")
            aa.append(aa[i])
        if aa[i].startswith("3"):
            aa[i] = aa[i].replace("3*", "")
            aa.append(aa[i])
            aa.append(aa[i])

    binary = []
    for i in range(0, 3):
        to_bin = aa[i]
        to_bin = to_bin.replace("a**", "")
        to_bin = int(to_bin)
        while to_bin >= len(a_def):
            to_bin -= len(a_def)
            # print(to_bin)
        binary.append(a_def[to_bin])
    return binary


def sumBin(binary):
    bin_0 = binary[0]
    bin_1 = binary[1]
    bin_2 = binary[2]
    result = []
    for i in range(0, 4):
        bin_sum = bin_0[i] + bin_1[i] + bin_2[i]
        if bin_sum == 1:
            result.append(1)
        elif bin_sum == 2:
            result.append(0)
        elif bin_sum == 3:
            result.append(1)
        elif bin_sum == 0:
            result.append(0)
    return result


def reduce(power):
    if power > 4:
        operations = power - 4
        return [f"x**{operations + 1}", f"x**{operations}"]
    else:
        return [f"x**{power}"]


def matchA_def(binary, a_def):
    binary = str(binary)
    print(f"a_def binary used: {binary}")
    for i in range(0, len(a_def)):
        if str(a_def[i]) == binary:
            return i


def reduceAlpha(i):
    parts = [i]
    # print(parts)
    # quit()
    reduced_parts = []
    reduced_count = 0
    while True:
        while len(parts) != 0:
            part = parts[0]
            # print(f"reducing: {part}")
            parts.remove(part)
            if part > 3:
                new_part_1 = part - 3
                new_part_2 = part - 4
                if new_part_1 not in reduced_parts:
                    reduced_parts.append(new_part_1)
                else:
                    reduced_parts.remove(new_part_1)

                if new_part_2 not in reduced_parts:
                    reduced_parts.append(new_part_2)
                else:
                    reduced_parts.remove(new_part_2)
                # print(f"reduced: {reduced_parts}")
            else:
                if part not in reduced_parts:
                    reduced_parts.append(part)
                else:
                    reduced_parts.remove(part)
        for part in reduced_parts:
            if part < 4:
                reduced_count += 1
        # print("\n")
        # print(reduced_parts)
        # print(reduced_count)
        if reduced_count == len(reduced_parts):
            # print(reduced_parts)
            break
        else:
            parts = reduced_parts
            reduced_parts = []
            reduced_count = 0
            # print(parts)
            # print(reduced_parts)
        # quit()

    result = ""
    # reducedParts = list(set(reducedParts))
    # print(parts)
    for i in range(0, len(reduced_parts)):
        # print(parts[i])
        result += "x**" + str(reduced_parts[i])
        if i < len(reduced_parts) - 1:
            result += "+"
        # print(result)
    return result


def reduceToMin(i):
    op = i - 4
    parts = [1 + op, op]
    reducedParts = []
    while len(parts) != 0:
        for part in parts:
            if part < 4:
                parts.remove(part)
                if part in reducedParts:
                    reducedParts.remove(part)
                else:
                    reducedParts.append(part)
            elif part == 4:
                parts.remove(part)
                if 1 in reducedParts:
                    reducedParts.remove(1)
                else:
                    reducedParts.append(1)  # =x
                if 0 in reducedParts:
                    reducedParts.remove(0)
                else:
                    reducedParts.append(0)  # =1
            else:
                parts.remove(part)
                op = part - 4
                if 1 + op in reducedParts:
                    reducedParts.remove(1 + op)
                else:
                    parts.append(1 + op)
                if op in reducedParts:
                    reducedParts.remove(op)
                else:
                    parts.append(op)
    return reducedParts


def convertBinToA(binary, a_def):
    i = matchA_def(binary, a_def)
    result = reduceToMin(i)
    return result


def invertAlpha(index, a_def):
    return len(a_def) - index


def binMatToAPower(M, a_def):
    M_shape = M.shape
    rows = M_shape[0]
    columns = M_shape[1]
    for i in range(0, rows):
        for j in range(0, columns):
            if M[i][j] != 0:
                a = matchA_def(M[i][j], a_def)
                # print(a)
                if a is None:
                    M[i][j] = 0
                else:
                    M[i][j] = a + 1
    return M


def smoothMat(M, threshold):
    M_shape = M.shape
    rows = M_shape[0]
    columns = M_shape[1]
    for i in range(0, rows):
        for j in range(0, columns):
            a = M[i][j]
            while a > threshold:
                a -= threshold
            # M[i][j] = a_def[a]
            M[i][j] = a - i
    return M


def powerMatToAlpha(M, a_def):
    M_shape = M.shape
    rows = M_shape[0]
    columns = M_shape[1]
    for i in range(0, rows):
        for j in range(0, columns):
            a = M[i][j]
            # while a > 14:
            #    a-=14
            if a is None:
                M[i][j] = [0, 0, 0, 0]
            else:
                M[i][j] = a_def[a]
            # M[i][j] = a
    return M


def extractBin(M):
    M_shape = M.shape
    rows = M_shape[0]
    columns = M_shape[1]
    print(f"rows={rows}")
    print(f"columns={columns}")
    N = np.empty((8, 12), dtype=int)
    for i in range(0, rows):
        for j in range(0, columns):
            b = M[i][j]
            # print(b)
            for k in range(1, 5):
                # print((i*4)+k)
                N[(i * 4) + k - 1][j] = b[k - 1]
    return N


def nullspaceToGenMat(H):
    H_shape = H.shape
    rows = H_shape[0]
    columns = H_shape[1]
    # print(rows)
    G = np.empty((columns, rows), dtype=int)
    for i in range(0, columns):
        binary = ''
        for j in range(0, rows):
            binary += str(H[j][i])
        binary = int(binary, 2)
        binary = binary % 2
        binary = bin(binary)
        print(binary)
    return G


def factorsToBin(factors):
    print(f"factors: {factors}")
    binary = [0] * 4
    for i in factors:
        if i.startswith("1"):
            binary[0] = 1
        else:
            i = int(i.replace("x**", ""))
            if i == 0:
                binary[0] = 1
            elif i == 1:
                binary[1] = 1
            elif i == 2:
                binary[2] = 1
            elif i == 3:
                binary[3] = 1
    return binary


def reduceMat(M, a_def):
    M_shape = M.shape
    rows = M_shape[0]
    columns = M_shape[1]
    N = np.empty((8, 12), dtype=int)
    for i in range(0, rows):
        for j in range(0, columns):
            b = M[i][j]
            # print(b)
            if b > 14:
                b = reduceAlpha(b)
                factors = str(b).split("+")
                binary = factorsToBin(factors)
                alpha = matchA_def(binary, a_def)
                b = alpha
            M[i][j] = b
    return M


def multiplicateMat(X, Y, Z, a_def):
    x_shape = X.shape
    y_shape = Y.shape
    rows = x_shape[0]
    columns = y_shape[1]
    H = np.zeros((rows, columns), dtype=list)
    for i in range(0, rows):
        for j in range(0, columns):
            res = []
            for k in range(0, rows):
                fac_1 = X[i, k]
                fac_2 = Y[k, j]
                if fac_2 == 1 or fac_1 == 0 or fac_1 == 1 or fac_2 == 0:
                    res.append(fac_1 * fac_2)
                else:
                    res.append(fac_1 + fac_2)
            H[i, j] = res

    #     z_shape = Z.shape

    for i in range(0, rows):
        for j in range(0, columns):
            res = []
            for k in range(0, columns):
                fac_1 = H[i, k]
                fac_2 = Z[k, j]
                if fac_2 == 0:
                    continue
                else:
                    res.append(fac_1)
                    res.append(fac_2)
            H[i, j] = res

    for i in range(0, rows):
        for j in range(0, columns):
            for k in H[i, j]:
                if type(k) == list:
                    for l in k:
                        if l == 0:
                            k.remove(l)

    for i in range(0, rows):
        for j in range(0, columns):
            item = H[i, j]
            variables = item[0]
            multiplicator = item[1]
            result = []
            for k in variables:
                # k -= 1

                if k == 1:
                    result.append(multiplicator)
                else:
                    result.append(k + multiplicator)
            H[i, j] = result

    for i in range(0, rows):
        for j in range(0, columns):
            item = H[i, j]
            if len(item) == 1:
                H[i, j] = item[0]
            else:
                binaries = []
                for k in item:
                    if k > 14:
                        k = reduceAlpha(k)
                        k_factors = k.split("+")
                        k_bin = factorsToBin(k_factors)
                        binaries.append(k_bin)
                    else:
                        k_bin = a_def[k]
                        binaries.append(k_bin)
                bin_0 = binaries[0]
                bin_1 = binaries[1]

                result = []
                for l in range(0, 4):
                    bin_sum = bin_0[l] + bin_1[l]
                    if bin_sum == 1:
                        result.append(1)
                    elif bin_sum == 2:
                        result.append(0)
                    elif bin_sum == 3:
                        result.append(1)
                    elif bin_sum == 0:
                        result.append(0)

                H[i, j] = matchA_def(result, a_def)
    return H


def random_inv_matrix(n):
    for i in range(1, 1000):
        try:
            candidate = np.random.randint(2, size=(n, n))
            det = int(round(np.linalg.det(candidate)))
            if det % 2 == 1:
                return candidate
        except ValueError:
            pass
    return None


def random_perm_matrix(n):
    return np.array([[1 if i == x else 0 for i in range(n)] for x in np.random.permutation(n)])


def anyMatToBool(M):
    BoolMat = np.zeros((M.shape[0], M.shape[1]), dtype=bool)
    for i in range(0, M.shape[0]):
        for j in range(0, M.shape[1]):
            BoolMat[i, j] = bool(M[i, j])
    return BoolMat


def intMatFromBool(M):
    IntMat = np.zeros((M.shape[0], M.shape[1]), dtype=int)
    for i in range(0, M.shape[0]):
        for j in range(0, M.shape[1]):
            IntMat[i, j] = int(M[i, j])
    return IntMat


def correct_error(message, G):
    res = np.zeros((12, 1), dtype=int)
    for i in range(0, message.shape[0]):
        for j in range(0, message.shape[1]):
            res[j, i] = message[i, j]
        print(res)
    message = anyMatToBool(G).dot(anyMatToBool(res))
    print(message)
    return message


def generator_mat():
    field = GF(2 ** 4)

    a_def = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1], [1, 1, 0, 0], [0, 1, 1, 0], [0, 0, 1, 1],
             [1, 1, 0, 1], [1, 0, 1, 0], [0, 1, 0, 1], [1, 1, 1, 0], [0, 1, 1, 1], [1, 1, 1, 1], [1, 0, 1, 1],
             [1, 0, 0, 1]]

    """test"""
    # print(a**3)
    g = Poly(x ** 2 + x + a ** 3).set_domain(GF(2 ** 4))
    # print(g)

    aa = []
    for i in range(1, 13):
        aa.append(str(a ** (i + 1)))
    print(aa)

    a_list = []
    for i in range(1, 14):
        a_list.append(a_def[i])

    g_list = [a_list[5], [1, 0, 0, 0]]

    X = np.zeros((2, 2), dtype=list)
    for i in range(0, 2):
        for j in range(0, 2):
            if i == j:
                X[i][j] = g_list[1]
            elif j < i:
                X[i][j] = g_list[0 + j]

    Y = np.ones((2, 12), dtype=list)
    for i in range(0, 2):
        for j in range(0, 12):
            if i > 0:
                Y[i][j] = a_list[j]
            elif i == 0:
                Y[i][j] = [1, 0, 0, 0]

    Z = np.zeros((12, 12), dtype=list)
    for i in range(0, 12):
        for j in range(0, 12):
            if i == j:
                ge = str(g)
                ge = ge.replace(" x,", "")
                ge = ge.replace("x", f"({aa[i]})")
                ge = Poly(ge)
                binary = convertGEToBin(ge, a_def)
                sum_binary = sumBin(binary)
                # a_from_bin = convertBinToA(sum_binary, a_def)
                a_inv = invertAlpha(matchA_def(sum_binary, a_def), a_def)
                # print(a_inv)
                Z[i][j] = a_def[a_inv - 1]

    X = binMatToAPower(X, a_def)
    print(X)
    Y = binMatToAPower(Y, a_def)
    print(Y)
    Z = binMatToAPower(Z, a_def)
    print(Z)
    H = multiplicateMat(X, Y, Z, a_def)
    H = powerMatToAlpha(H, a_def)
    H = extractBin(H)

    X, nullity = nmod_mat(H.shape[0], H.shape[1], [int(e) for e in H.flatten()], 2).nullspace()
    X = np.array([int(e) for e in X.entries()]).reshape(X.nrows(), X.ncols())
    G = X.T[:nullity]
    G[[0, 1, 2, 3]] = G[[1, 0, 3, 2]]
    print(G)

    S = None
    while S is None:
        S = random_inv_matrix(4)

    """FAKE"""
    S = np.array([[1, 0, 0, 1], [0, 1, 0, 1], [0, 1, 0, 0], [0, 0, 1, 1]])
    print(S)

    S = anyMatToBool(S)

    P = random_perm_matrix(12)

    """Fake"""
    P = np.array([[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
                  [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
                  [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                  [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                  [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]])
    print(P)

    P = anyMatToBool(P)
    G = anyMatToBool(G)
    print(G)

    PublicKey = S.dot(G).dot(P)
    PublicKey = intMatFromBool(PublicKey)
    print(PublicKey)

    """Encrypt"""
    m = np.zeros((1, 4), dtype=int)
    m[0][0] = 1
    m[0][1] = 0
    m[0][2] = 1
    m[0][3] = 0
    print(m)
    m = anyMatToBool(m)
    print(m)

    new_m = anyMatToBool(m).dot(anyMatToBool(PublicKey))
    print(intMatFromBool(new_m))

    bits = np.random.choice(len(new_m[0]), 2, replace=False)
    """FAKE"""
    bits = np.array([[1], [1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]])
    print(bits)
    for i in range(0, m.shape[1]):
        new_m[0][i] = (new_m[0][i] + bits[i]) % 2
    print(new_m)

    y = intMatFromBool(new_m)
    print(f"chiphrate: {y}")
    # print(P)
    # print(P.__invert__())
    """Decrypt"""
    print(P)
    message = anyMatToBool(y).dot(anyMatToBool(np.linalg.inv(intMatFromBool(P))))
    print(intMatFromBool(message))

    print(intMatFromBool(G).T)
    print(intMatFromBool(message).T)
    someMat = np.hstack((intMatFromBool(G).T, intMatFromBool(message).T))
    print(f"someMat: {someMat}")
    print(Matrix(anyMatToBool(someMat)))
    msg, rows = Matrix(someMat).rref()
    print(msg)

    message = [[]]
    for row in range(0, len(rows)-1):
        message[0].append(msg.row(row).col(4)[0])
    print(message)

    message = anyMatToBool(np.array(message)).dot(intMatFromBool(S).T)
